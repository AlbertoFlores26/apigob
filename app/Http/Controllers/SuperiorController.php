<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Superior;
use App\Cliente;

class SuperiorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $token = $request->header('Authorization');
        $cliente = Cliente::all();
        $json = array();

        foreach ($cliente as $key => $value) {
            
            if("Basic ". base64_encode($value["ews_id_cliente"].":".$value["ews_llave_secreta"]) == $token){

                $solicitudsup = Superior::all();

        //Validar solicitudes
                if(!empty($solicitudsup)){


                 $json = array(

                        "wsp_status" =>200,
                        "wsp_total_registros" => count($solicitudsup),
                        "wsp_mensaje" => $solicitudsup
                    );

                     
                }else{

                $json = array(
                     
                     "wsp_status" =>200,
                     "wsp_total_registros" => 0,
                     "wsp_mensaje" => "No tienes solicitud de tramite."

                    );
                }

            }else{

                $json = array(
                     
                     "wsp_status" =>404,
                     "wsp_mensaje" => "No esta autorizado para recibir los registros."

                    );
            }
        }

        return json_encode($json, true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $token = $request->header('Authorization');
        $cliente = Cliente::all();
        $json = array();

        foreach ($cliente as $key => $value) {
            
            if("Basic ". base64_encode($value["ews_id_cliente"].":".$value["ews_llave_secreta"]) == $token){

        //Recoger datos
        
        $datos = array("ews_solici_formato" => $request->input("ews_solici_formato"), 
                       "ews_tema_nombre" => $request->input("ews_tema_nombre"),
                       "ews_acta_constitutiva" => $request->input("ews_acta_constitutiva"),
                       "ews_carta_obligato" => $request->input("ews_carta_obligato"),
                       "ews_anexo1" => $request->input("ews_anexo1"),
                       "ews_anexo3" => $request->input("ews_anexo3"),
                       "ews_mapa_curricular" => $request->input("ews_mapa_curricular"),
                       "ews_lista_acervos" => $request->input("ews_lista_acervos"),
                       "ews_regla_plantel" => $request->input("ews_regla_plantel"));
        //VALIDAR
        if(!empty($datos))
        {
             $validator = Validator::make($datos, [
                'ews_solici_formato' => 'required|string|max:255',
                'ews_tema_nombre' => 'required|string|max:255',
                'ews_acta_constitutiva' => 'required|string|max:255',
                'ews_carta_obligato' => 'required|string|max:255',
                'ews_anexo1' => 'required|string|max:255',
                'ews_anexo3' => 'required|string|max:255',
                'ews_mapa_curricular' => 'required|string|max:255',
                'ews_lista_acervos' => 'required|string|max:255',
                'ews_regla_plantel' => 'required|string|max:255',
            ]);

            //Si falla la validacion
             if ($validator->fails()) {
               $json = array(
                "wsp_status" =>404,
                "wsp_mensaje" => "Registros con errores: Es posible que el formulario este vacio"
            );

                return json_encode($json, true);

           }else{

            $solicitudsup = new Superior();
            $solicitudsup->ews_solici_formato = $datos["ews_solici_formato"];
            $solicitudsup->ews_tema_nombre = $datos["ews_tema_nombre"];
            $solicitudsup->ews_acta_constitutiva = $datos["ews_acta_constitutiva"];
            $solicitudsup->ews_carta_obligato = $datos["ews_carta_obligato"];
            $solicitudsup->ews_anexo1 = $datos["ews_anexo1"];
            $solicitudsup->ews_anexo3 = $datos["ews_anexo3"];
            $solicitudsup->ews_mapa_curricular = $datos["ews_mapa_curricular"];
            $solicitudsup->ews_lista_acervos = $datos["ews_lista_acervos"];
            $solicitudsup->ews_regla_plantel = $datos["ews_regla_plantel"];
            $solicitudsup->ews_id_creador = $value["id"];
            $solicitudsup->save();

                $json = array(
                "wsp_status" =>200,
                "wsp_mensaje" => "Registro exitoso, su solicitud ha sido enviado."
            );


           }

           
            
        }else{

            $json = array(
                "wsp_status" =>404,
                "wsp_mensaje" => "Los registros no pueden estar vacios."
            );

        }

            }
        }

        return json_encode($json, true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $token = $request->header('Authorization');
        $cliente = Cliente::all();
        $json = array();

        foreach ($cliente as $key => $value) {
            
            if("Basic ". base64_encode($value["ews_id_cliente"].":".$value["ews_llave_secreta"]) == $token){

                $solicitudsu = Superior::where("id", $id)->get();

        //Validar cursos
                if(!empty($solicitudsu)){


                 $json = array(

                        "wsp_status" =>200,
                        "wsp_mensaje" => $solicitudsu
                    );

                     
                }else{

                $json = array(
                     
                     "wsp_status" =>200,
                     "wsp_mensaje" => "No hay solicitud en la tabla"

                    );
                }

            }else{

                $json = array(
                     
                     "wsp_status" =>404,
                     "wsp_mensaje" => "No esta autorizado para ver estas solicitudes."

                    );
            }

        }

        return json_encode($json, true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $token = $request->header('Authorization');
        $cliente = Cliente::all();
        $json = array();

        foreach ($cliente as $key => $value) {
            
            if("Basic ". base64_encode($value["ews_id_cliente"].":".$value["ews_llave_secreta"]) == $token){

        //Recoger datos
        
         $datos = array("ews_solici_formato" => $request->input("ews_solici_formato"), 
                       "ews_tema_nombre" => $request->input("ews_tema_nombre"),
                       "ews_acta_constitutiva" => $request->input("ews_acta_constitutiva"),
                       "ews_carta_obligato" => $request->input("ews_carta_obligato"),
                       "ews_anexo1" => $request->input("ews_anexo1"),
                       "ews_anexo3" => $request->input("ews_anexo3"),
                       "ews_mapa_curricular" => $request->input("ews_mapa_curricular"),
                       "ews_lista_acervos" => $request->input("ews_lista_acervos"),
                       "ews_regla_plantel" => $request->input("ews_regla_plantel"));
        //VALIDAR
        if(!empty($datos))
        {
            $validator = Validator::make($datos, [
                'ews_solici_formato' => 'required|string|max:255',
                'ews_tema_nombre' => 'required|string|max:255',
                'ews_acta_constitutiva' => 'required|string|max:255',
                'ews_carta_obligato' => 'required|string|max:255',
                'ews_anexo1' => 'required|string|max:255',
                'ews_anexo3' => 'required|string|max:255',
                'ews_mapa_curricular' => 'required|string|max:255',
                'ews_lista_acervos' => 'required|string|max:255',
                'ews_regla_plantel' => 'required|string|max:255',
            ]);

            //Si falla la validacion
             if ($validator->fails()) {
               $json = array(
                "wsp_status" =>404,
                "wsp_mensaje" => "Registros con errores:"
            );

                return json_encode($json, true);

           }else{

                $traer_solicitud = Superior::where("id", $id)->get();

                if($value["id"] == $traer_solicitud[0]["ews_id_creador"]){

                    $datos = array("ews_solici_formato" => $datos["ews_solici_formato"],
                                   "ews_tema_nombre" => $datos["ews_tema_nombre"],
                                   "ews_acta_constitutiva" => $datos["ews_acta_constitutiva"],
                                   "ews_carta_obligato" => $datos["ews_carta_obligato"],
                                   "ews_anexo1" => $datos["ews_anexo1"],
                                   "ews_anexo3" => $datos["ews_anexo3"],
                                   "ews_mapa_curricular" => $datos["ews_mapa_curricular"],
                                   "ews_lista_acervos" => $datos["ews_lista_acervos"],
                                   "ews_regla_plantel" => $datos["ews_regla_plantel"]);

                    $solicitudsup = Superior::where("id", $id)->update($datos);

                    $json = array(
                "wsp_status" =>200,
                "wsp_mensaje" => "Registro exitoso, su solicitud ha sido actualizado."
            );
                }else{

                $json = array(
                "wsp_status" =>404,
                "wsp_mensaje" => "No esta autorizado para editar esta solicitud."
            );
                }

                


           }

           
            
        }else{

            $json = array(
                "wsp_status" =>404,
                "wsp_mensaje" => "Los registros no pueden estar vacios."
            );

        }

            }
        }

        return json_encode($json, true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $token = $request->header('Authorization');
        $cliente = Cliente::all();
        $json = array();

        foreach ($cliente as $key => $value) {
            
            if("Basic ". base64_encode($value["ews_id_cliente"].":".$value["ews_llave_secreta"]) == $token){

                $validar = Superior::where("id", $id)->get();

                if(!empty($validar)){

            if($value["id"] == $validar[0]["ews_id_creador"])

                {
                
                    $solicitudsup = Superior::where("id", $id)->delete();

                     $json = array(
                     
                     "wsp_status" =>200,
                     "wsp_mensaje" => "Su curso se ha borrado con exito"

                    );
                return json_encode($json, true);

                }else{

                     $json = array(
                     
                     "wsp_status" =>404,
                     "wsp_mensaje" => "No esta autorizado a borrar este registro."

                    );

                return json_encode($json, true);
                }

                     
                }else{

                $json = array(
                     
                     "wsp_status" =>404,
                     "wsp_mensaje" => "No hay cursos"

                    );
                return json_encode($json, true);
                }

            }
        }

        return json_encode($json, true);
    }
}
